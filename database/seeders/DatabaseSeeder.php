<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\Product;
use App\Models\User;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Order::query()->delete();
        Product::query()->delete();
        User::query()->delete();

        $users = User::factory(5)->create();

        $products = Product::factory(10)->create();

        $orders = Order::factory(10)
            ->state(new Sequence(
                static function (Sequence $sequence) use ($users) {
                    return [
                        'user_id' => $users->random()->id,
                    ];
                }
            ))
            ->create();

        /** @var Order $order */
        foreach ($orders as $order) {
            for ($i = 0; $i <= rand(1, 3); $i++) {
                $order->products()->attach($products->random()->id, [
                    'quantity' => fake()->randomDigitNotZero(),
                    'price'    => fake()->randomDigitNotZero(),
                ]);
            }
        }

        echo User::query()->first()?->createToken('Test token')?->plainTextToken;
    }
}
