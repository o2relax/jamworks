<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class StoreOrderRequest extends FormRequest
{

    public function rules(): array
    {
        return [
            'products'              => ['required', 'array'],
            'products.*.product_id' => ['required', 'exists:' . Product::class . ',id'],
            'products.*.quantity'   => ['required', 'numeric', 'min:1'],
            'products.*.price'      => ['required', 'numeric'],
        ];
    }
}
