<?php

namespace App\Http\Controllers\Api;

use App\Enums\OrderStatuses;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function index(Request $request)
    {
        return response()->json(
            OrderResource::collection(
                Order::with(['user', 'products'])
                    ->paginate($request->input('perPage', 20))
            )
        );
    }

    public function show(Order $order)
    {
        return response()->json(
            new OrderResource(
                $order->load(['user', 'products'])
            )
        );
    }

    public function store(StoreOrderRequest $request)
    {
        /** @var Order $order */
        $order = Order::query()->create(['status' => OrderStatuses::New->value]);
        $order->products()->attach($request->input('products'));

        return response()->json(['success' => true]);
    }

    public function update(StoreOrderRequest $request, Order $order)
    {
        $order->products()->delete();
        $order->products()->attach($request->input('products'));

        return response()->json(['success' => true]);
    }

    public function destroy(Order $order)
    {
        $order->delete();

        return response()->json(['success' => true]);
    }
}
