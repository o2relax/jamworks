<?php

namespace App\Enums;

enum OrderStatuses: int
{
    case New = 1;
    case Completed = 2;
}
