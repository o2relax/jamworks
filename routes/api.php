<?php

use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('orders', [OrderController::class, 'store'])->name('api.orders.store');

Route::middleware(['auth:sanctum'])->group(function () {
    Route::resource('orders', OrderController::class);
    Route::resource('products', ProductController::class);
});

Route::post('tokens/create', function (Request $request) {
    $token = $request->user()->createToken($request->token_name);

    return response()->json(['token' => $token->plainTextToken]);
});
